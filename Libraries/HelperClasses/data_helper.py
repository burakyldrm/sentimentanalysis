'''
Created on March 29, 2018
@author: burak yildirim
'''
import pandas, os, time, numpy
from abc import ABCMeta, abstractmethod

class dataHelper(object):
    '''
    This class uses for help to reading data.
    '''
    def __init__(self, folder = '../../Data/core_data'):
        """
        :param folder: Folder path where data is located to read
        """
        print(" * "*20)
        self.folder = folder
        self.path   = os.path.join(os.path.split(os.path.abspath(__file__))[0], self.folder)
        self._files = os.listdir(self.path)

    def read_data(self):
        '''
        :return: self.instances, self.labels
        '''
        print("\x1b[1mData path \t\t: \x1b[0m", self.path)

        try:
            print('\x1b[1;91m' + "Data File Names \x1b[1m:" + '\x1b[0m')

            veri = []
            t = time.time()
            for i, file in enumerate(self._files):
                print("\t{}* '\x1b[1m{}\x1b[0m' dosyası okunuyor..".format(i + 1, file))
                t1 = time.time()
                instances, labels = self._read_excel(file)
                print("\t\tNumber of Instance : \x1b[1;94m{}\x1b[0m".format(len(instances)))
                veri.append(pandas.DataFrame({'text': instances, 'polarity': labels}))
                t2 = time.time()
                print("\t\t\x1b[1;94m{0:0.3f}\x1b[0m saniyede okundu.".format((t2 - t1)))

            print('\x1b[1m' + "Number of Data File \t \x1b[1m:" + '\x1b[0m', len(self._files))
            print("\x1b[1mToplam okuma süresi \t :\x1b[0m {0:0.3f} sec.".format(time.time() - t))
            veri = pandas.concat(veri, ignore_index=True)
            self.veri = veri
            print("\x1b[1mTotal Number of Instance :\x1b[0m {:,}".format(len(veri)))

            # print("Önceki : ", len(veri))
            veri['text'].replace('', numpy.nan, inplace=True)
            veri['text'].replace('nan', numpy.nan, inplace=True)
            veri.dropna(inplace=True)
            # print("Sonraki : ", len(veri))
            self.instances = veri.text
            self.labels = veri.polarity

            return self.instances, self.labels

        except:
            print("Geçmiş olsun. Veri dosyalarını okumada sıkıntı var!")

    # @abstractmethod
    def add_data(self, filename, X, y):
        '''
        :param filename:
        :param X:
        :param y:
        :return: instances, labels
        '''
        veri = pandas.read_excel(filename)
        instances = veri[X].values.tolist()
        instances = [str(i) for i in instances]
        labels = veri[y].values.tolist()
        instances = [i.strip() for i in instances]

        if self.veri is not None:
            veri = pandas.DataFrame({'text': instances, 'polarity': labels})
            veri = pandas.concat([veri, self.veri], ignore_index=True)
            self.veri = veri
            self.instances = veri.text
            self.labels = veri.polarity

            return self.instances, self.labels

        return instances, labels

    def _read_excel(self, path):
        '''
        :param path: path where the data to be read is located
        :return: instances, labels
        '''
        path      = os.path.join(self.path, path)
        veri      = pandas.read_excel(path, header=None)
        instances = veri[1].values.tolist()
        instances = [str(i) for i in instances]
        labels    = veri[0].values.tolist()
        instances = [i.strip() for i in instances]

        return instances, labels

    def _read_csv(self, path):
        pass

if __name__ == '__main__':
    pass