'''
Created on March 17, 2018
@author: burak yıldırım
'''
import pandas, os
import Sentiment.Helpers.sentiment_helpers as funcs
from Libraries.HelperClasses.data_helper import *
from Sentiment.Tasks.SentimentAnalysis import SentimentAnalysis

path = os.path.join(os.path.split(os.path.abspath(__file__))[0], '../../Data')

if __name__ == '__main__':

    heyyou              = SentimentAnalysis()
    folder              = '../../Data/duzenlenecekler/core_data'
    instances, labels   = heyyou.getAllData(folder=folder)

    heyyou.save_data('/home/burak/Desktop/urlsRegex.xlsx')
    heyyou.deneme2()

    # # # # # # # # # # #

    # Öncelikle Verilerin bulunduğu klasör..
    # folder = funcs.setdatafolder('Data/duzenlenecekler/core_data')

    # Sentiment Class oluşturulur..
    # tr_sentiment = SentimentAnalysis()

    # veriler cağırılır.
    # instances, labels = tr_sentiment.getAllData()

