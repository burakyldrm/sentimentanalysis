import pandas, re, numpy
from gensim.models import Word2Vec
from nltk.corpus import gutenberg
from multiprocessing import Pool
from scipy import spatial
from gensim.models import Doc2Vec
from gensim.models.doc2vec import TaggedDocument

mapping = {
    'ilgisiz': 'neutral',
    'Positive': 'positive',
    'positive': 'positive',
    'Negative': 'negative',
    'negative': 'negative',
    'Neutral': 'neutral',
    'neutral': 'neutral',
    'Karar Veremedim': 'neutral',
    'Olumsuz': 'negative',
    'olumsuz': 'negative',
    'Olumlu': 'positive',
    'olumlu': 'positive',
    'Nötr': 'neutral',
    'nötr': 'neutral',
}
veri = pandas.read_excel('/home/burak/Desktop/urlsRegex.xlsx')
docs = veri[1]
labs = veri[0].map(mapping).map({'neutral':0, 'negative':1, 'positive':2}).tolist()

if __name__ == '__main__':
    # sentences = list(gutenberg.sents('shakespeare-hamlet.txt'))
    # print(sentences)
    sentences = []
    for doc in docs:
        sentences.append(doc.split())
        # print(doc.split())
    # print(sentences)

    for i in range(len(sentences)):
        sentences[i] = [word.lower() for word in sentences[i] if re.match('^[a-zA-Z]+', word)]

    for i in range(len(sentences)):
        sentences[i] = TaggedDocument(words=sentences[i], tags=['sent{}'.format(i)])

    model = Doc2Vec(documents=sentences, dm=1, size=100, window=3, min_count=1, iter=10, workers=Pool()._processes)
    model.init_sims(replace=True)

    v1 = model.infer_vector('sent2')  # in doc2vec, infer_vector() function is used to infer the vector embedding of a document
    v2 = model.infer_vector('sent3')

    print(model.most_similar([v1]))