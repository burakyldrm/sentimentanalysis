'''
Created on March 29, 2018
@author: burak yildirim
'''
import pandas, os, time, re, numpy
from Libraries.HelperClasses.data_helper import dataHelper
from Libraries.HelperClasses.preprocess_helper import preprocess
from turkish.deasciifier import Deasciifier
from abc import ABCMeta, abstractmethod

class dataCore(dataHelper, preprocess):
    '''
    Base Class for reading, adding, preprocessin data
    Attributes:
        :param :
    '''
    # __metaclass__   = ABCMeta
    folder          = None
    drop_duplicate  = None

    def get_and_process_data(self):
        ''' read, get and preprocess '''
        print("data_core calisiyor")
        self.read_data()

    # @abstractmethod
    def add_data(self):
        print("Burak Yıldırım Deneme..")

    def dene1(self):
        instances, labels = self.read_data()

        a = []
        for instance in instances:
            # print(self.data_cleaner(instance))
            # print(re.split('\W+', self.data_cleaner(instance.lower())))
            a.append(re.split('\W+', self.data_cleaner(instance.lower())))

        # print(a)

        self.instances = []
        for i in a:
            self.instances.append(Deasciifier(" ".join(i)).convert_to_turkish())
            print(Deasciifier(" ".join(i)).convert_to_turkish())

        self.instances = pandas.Series(self.instances).tolist()

        veri = pandas.DataFrame({1:self.instances, 0:self.labels})
        self.veri = veri
        # veri.to_csv('/home/burak/Desktop/denemeeee.xlsx', sep='\t')

    def dene2(self):
        instances, labels = self.read_data()
        a = []
        for instance in instances:
            # print(self.tag_emoticons(instance))
            word = self.tag_emoticons(instance.lower())
            word = self.data_cleaner(word)
            a.append(re.split('\W+', word))

        self.instances = []
        for i in a:
            word = " ".join(i)
            word = Deasciifier(word).convert_to_turkish()
            self.instances.append(word)
            print(word)

        self.instances = pandas.Series(self.instances).tolist()

        veri = pandas.DataFrame({1: self.instances, 0: self.labels})
        self.veri = veri
        # veri.to_csv('/home/burak/Desktop/denemeeee.xlsx', sep='\t', index=False)

    def get_all_and_process(self):
        instances, labels = self.read_data()

        dictionary = []
        for instance in instances.tolist():
            for word in instance.split():
                dictionary.append(word)
        print(len(dictionary))
        print(len(set(dictionary)))

        words = []
        for word in set(dictionary):
            words.append(word)

        print(words)

        sonlist = []
        for word in words:
            word = word.replace('"', ' ')
            sonlist.extend(re.split(r'[(:.,;!-\\\'+)]', word))
            # print(re.split(r'[(:.,;-\\\'+)]', word))

        for word in sonlist:
            if not word == '':
                print(word)

    def ready_to_go(self):
        '''

        :return: self.instances, self.labels
        '''
        if self.folder:
            super().__init__(self.folder)

        instances, labels = self.read_data()
        t1 = time.time()
        # print("Data Preprocess is initiating..")
        print("Veri Temizleme başlatılıyor ..")
        instances = self.cleanerV3(instances)
        self.veri = pandas.DataFrame({1: instances, 0: self.labels})
        self.veri[1].replace('', numpy.nan, inplace=True)
        self.veri[1].replace('nan', numpy.nan, inplace=True)
        self.veri.dropna(inplace=True)

        if self.drop_duplicate is True:
            print("\t* Dropping Duplicate Data..")
            self.veri.drop_duplicates(subset=[1], inplace=True)
            self.veri       = self.veri.reset_index()
            self.instances  = self.veri[1]
            self.labels     = self.veri[0]
            self.veri       = pandas.DataFrame({
                                    1: self.instances,
                                    0: self.labels
                                })

        print("Veri Temizliği \x1b[1;91m{0:0.3f}\x1b[0m saniye sürdü.".format((time.time()-t1)))
        self.instances = self.veri[1]
        self.labels    = self.veri[0]

        return self.instances, self.labels

    def to_excel(self, filename, sep, noindex):
        '''
        :param filename:
        :param sep:
        :param index:
        :return:
        '''
        path = os.path.split(filename)
        print("\x1b[1m'{}'\x1b[0m dosyası \x1b[1m'{}'\x1b[0m adresine yazılıyor.. ".format(path[1], path[0]))
        self.veri.to_csv(filename, sep=sep, index=noindex)
        print("{} yazıldı..".format(path[1]))

if __name__ == '__main__':

    d = dataCore()
    # instances, labels = d.ready_to_go()
    d.add_data()