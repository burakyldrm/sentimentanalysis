import pandas, re, numpy
from gensim.models import Word2Vec
from nltk.corpus import gutenberg
from multiprocessing import Pool
from scipy import spatial

mapping = {
    'ilgisiz': 'neutral',
    'Positive': 'positive',
    'positive': 'positive',
    'Negative': 'negative',
    'negative': 'negative',
    'Neutral': 'neutral',
    'neutral': 'neutral',
    'Karar Veremedim': 'neutral',
    'Olumsuz': 'negative',
    'olumsuz': 'negative',
    'Olumlu': 'positive',
    'olumlu': 'positive',
    'Nötr': 'neutral',
    'nötr': 'neutral',
}
veri = pandas.read_excel('/home/burak/Desktop/urlsRegex.xlsx')
docs = veri[1]
labs = veri[0].map(mapping).map({'neutral':0, 'negative':1, 'positive':2}).tolist()

if __name__ == '__main__':
    # sentences = list(gutenberg.sents('shakespeare-hamlet.txt'))
    # print(sentences)
    sentences = []
    for doc in docs:
        sentences.append(doc.split())
        # print(doc.split())
    # print(sentences)

    for i in range(len(sentences)):
        sentences[i] = [word.lower() for word in sentences[i] if re.match('^[a-zA-Z]+', word)]

    # print(sentences[777])

    model = Word2Vec(sentences=sentences, size=100, sg=1, window=3, min_count=1, iter=10, workers=Pool()._processes)
    model.init_sims(replace=True)

    print(model.most_similar('boşluk'))
