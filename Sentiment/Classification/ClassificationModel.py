'''
Created on April 11, 2018
@author: burak yildirim
'''
import pandas
from Libraries.CoreClasses.data_core import dataCore
from Libraries.CoreClasses.model_core import modelCore

class _Classification(object):
    d_core          = dataCore()
    m_core          = modelCore()

    def __init__(self):
        pass

    def getAllData(self, folder=None, duplicate=True):
        """
        This function use for getting all data that all we have
        :param folder: specific folder path to get data to read
        :param duplicate: we need to use if we want to remove duplicate data. Change "False" to "True"
        :return: instances, labels
        """
        self.d_core.folder          = folder
        self.d_core.drop_duplicate  = duplicate
        instances, labels           = self.d_core.ready_to_go()

        return instances, labels

    def save_data(self, filename, sep='\t', index=False, drop=False):
        '''
        Save the data that we used for training.
        :param filename: data file's name or data file's name with path that we want to save the data
        :param sep:
        :param index:
        :return:
        '''
        self.d_core.drop = drop
        self.d_core.to_excel(filename=filename, sep=sep, noindex=index)

    def build_model(self):
        """

        :return: model
        """
        self.m_core.train()

    def train_and_save_model(self, veri):
        """
        :param veri:
        :return: modelfolder
        """
        learning_model  = self.m_core.train(self.d_core.instances, self.d_core.labels)
        model_folder    = self.m_core._classification_system(learning_model, "picklefolder")

    def predict_online(self):
        """

        :return: prediction, pred_labels
        """
        pass

    def predict_offline(self):
        """

        :return: prediction, pred_labels
        """
        pass

    def deneme(self):
        print("burak")