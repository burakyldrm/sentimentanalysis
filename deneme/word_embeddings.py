import pandas, numpy
from keras.preprocessing.text import one_hot
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers.embeddings import Embedding
mapping = {
    'ilgisiz': 'neutral',
    'Positive': 'positive',
    'positive': 'positive',
    'Negative': 'negative',
    'negative': 'negative',
    'Neutral': 'neutral',
    'neutral': 'neutral',
    'Karar Veremedim': 'neutral',
    'Olumsuz': 'negative',
    'olumsuz': 'negative',
    'Olumlu': 'positive',
    'olumlu': 'positive',
    'Nötr': 'neutral',
    'nötr': 'neutral',
}
veri = pandas.read_excel('/home/burak/Desktop/urlsRegex.xlsx')
docs = veri[1].tolist()
labs = veri[0].map(mapping).map({'neutral':0, 'negative':1, 'positive':2}).tolist()

vocab_size = 50
encoded_docs = [one_hot(d, vocab_size) for d in docs]

max_length = 25
padded_docs = pad_sequences(encoded_docs, maxlen=max_length, padding='post')

model = Sequential()
model.add(Embedding(vocab_size, 8, input_length=max_length))
model.add(Flatten())
model.add(Dense(1, activation='sigmoid'))
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

model.fit(padded_docs, labs, epochs=50, verbose=0)
loss, accuracy = model.evaluate(padded_docs, labs, verbose=0)


if __name__ == '__main__':
    print(padded_docs)
    print("accuracy : ", accuracy*100)