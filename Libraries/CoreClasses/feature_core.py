'''
created on April 10, 2018
@author: burak yildirim
'''
import pandas
import sklearn.pipeline as sklearn_pipeline
import sklearn.feature_extraction.text as sklearn_text
import sklearn.linear_model as sklearn_linear

class featureExtraction(object):
    '''

    '''
    def __init__(self):
        '''
        ayarlar..
        '''
        pass

    def feature_extraction(self):
        '''
        tfidf vs
        :return:
        '''
        tfidfVec = sklearn_text.TfidfVectorizer()
        classifier = sklearn_linear.SGDClassifier()
        pipe1 = sklearn_pipeline.Pipeline([
            ('tfidf', tfidfVec),
            ('classifier', classifier)
        ])

        return classifier

if __name__ == '__main__':

    feature = featureExtraction()
    print(feature.feature_extraction())
    model = feature.feature_extraction()