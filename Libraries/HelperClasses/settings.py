'''
Created on March 17, 2018
@author: burak yıldırım
'''
import os


data_path = os.path.join(os.path.split(os.path.abspath(__file__))[0], '../Data')
# data_path2= os.path.join(os.getcwd(), '../data')


class color:

   def __init__(self):
      self.PURPLE = '\033[95m'
      self.CYAN = '\033[96m'
      self.DARKCYAN = '\033[36m'
      self.BLUE = '\033[94m'
      self.GREEN = '\033[92m'
      self.YELLOW = '\033[93m'
      self.RED = '\033[91m'
      self.BOLD = '\033[1m'
      self.UNDERLINE = '\033[4m'
      self.END = '\033[0m'

   def colors(self, gelen, ozellik=False):
      print(self.__dict__.keys())
      if ozellik:
         if ozellik in self.__dict__.keys():
            pass
         return 'yok'
      gelen = self.BOLD+gelen+self.END
      return gelen

# '''Ornek kullanımı'''
# print(color.BOLD + 'Hello World !' + color.END)


if __name__ == '__main__':
#     print(data_path)
#     print(data_path2)

      color = color()
      print(color.colors("asda"))