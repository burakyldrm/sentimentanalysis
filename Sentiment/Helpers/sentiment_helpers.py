'''
Created on April 11, 2018
@author: burak yildirim
'''
import pandas, os

def setdatafolder(path, defaultpath=False):
    if defaultpath is False:
        return os.path.join('../..', path)

    return os.path.join(defaultpath, path)

if __name__ == '__main__':
    pass
    # print(setdatafolder('a'))