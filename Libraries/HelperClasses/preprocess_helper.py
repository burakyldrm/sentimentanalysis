'''
created on March 29, 2018
@author: burak yildirim
'''
import pandas, re
from turkish.deasciifier import Deasciifier

class preprocess(object):
    '''
    With this class we can manage the data preprocess stage.
    Data cleaning, tagging and regex stages..
    :return: instances
    '''
    def cleanerV2(self, instances):
        '''
        V2
        :param instances:
        :return:
        '''
        list = []
        print("\t* Regex Uygulanıyor..")
        print("\t* Deasciify Uygulanıyor..")
        for instance in instances:
            word = self.regex(instance.lower())
            word = self.tag_emoticons(word)

            for instanc in [re.split('\W+', word)]:
                word = " ".join(instanc)
                word = Deasciifier(word).convert_to_turkish()
                list.append(word)
        #         print(word)
        # print(len(list))
        return pandas.Series(list).tolist()

    def cleanerV3(self, instances):
        '''
        This func uses for cleaning data.
        :param instances:
        :return: pandas.Series(list)
        '''
        dict={
            'Ç':'c', 'ç':'c', 'I':'i', 'İ':'i', 'Ö':'o', 'Ş':'s',
        }
        regex = re.compile('(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})')
        print("\t* Regex Uygulanıyor..")
        list=[]
        for instance in instances:
            for key, value in dict.items():
                instance = instance.replace(key, value)
            instance = instance.lower()
            instance = re.sub(regex, '', instance)
            instance = re.sub('(?:\@+[\w_]+[\w\'_\-]*[\w_]+)', '', instance)
            # instance = re.sub('(?:\#+[\w_]+[\w\'_\-]*[\w_]+)', '', instance)
            instance = re.sub('(?:^|\W)rt|(?:^|\W)at', '', instance)
            instance = self.tag_emoticons(instance)
            instance = re.sub('(?:[\W_]+)', ' ', instance)
            instance = re.sub(r'([a-z])\1+', r'\1', instance)
            instance = re.sub("(?:[0-9'\-_])", '', instance)
            instance = Deasciifier(instance).convert_to_turkish()
            list.append(instance)
        print("\t\t- Regex uygulandı")
        return pandas.Series(list).tolist()

    def regex(self, text):
        '''
        Veri temizleme işlemi için kullanılacak regexlerin yazılması
        :param text:
        :return: applied_regex
        '''
        dict = {
            '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})': 'URLs',
            '(?:\@+[\w_]+[\w\'_\-]*[\w_]+)': ' ',  # Mention ..
            '(?:\#+[\w_]+[\w\'_\-]*[\w_]+)': ' ',  # Hashtag..
            '(?:^|\W)rt': ' ',  # Remove spesific word rt
            '(?:^|\W)at': ' ',  # Remove spesific word at
            '(?:^|\s):': ' ',  # ikinokta üstü üste şeysi..
            "(?:[0-9'\-_])": " ",
            # '(?:[\W_]+)': ' ',  # punctuation (:,.{[(%&+ ..
            # r'([a-z])\1{1}': ' ',  # remove repeat letter r'([a-z])\1+'
            # "[^\w\s]": " ",  # punctuation

            # r"(?:http|ftp|https)://(?:[\w_-]+(?:(?:\.[\w_-]+)+))(?:[\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?": "fulya",
            # "@([\w]+)": " "
        }
        regex = re.compile("%s" % "|".join(dict.keys()))
        applied_regex = re.sub(r'([a-z])\1+', r'\1', re.sub(regex, '', text))
        return applied_regex

    def query_text(self, any_string):
        """
        Do not use pipe, search for each character in a text one by one.
        lil bit dummy but rgx pipe is much dummier.
        @Todo: Make this replace stuff a separate function
        """
        search_list      = self.emoticons().keys()
        emo_dict         = self.emoticons()
        emoticons_in_txt = []

        for emo in search_list:
            pattern = "(" + emo.replace("(", "\\(").replace(")", "\\)") \
                .replace("|", "\\|").replace("[", "\\[").replace("]", "\\]") + ")"
            m = re.findall(pattern, any_string)
            if emo in m:
                emoticons_in_txt.append(emo_dict[emo])
        return emoticons_in_txt

    def emoticons(self):
        """
        @Todo: Extend the list sometime.
        :return: emoticons
        """
        emoticons = {
            ':)': 'HAPPYFACE',
            ':]': 'HAPPYFACE',
            '.)': 'HAPPYFACE',
            '=)': 'HAPPYFACE',
            '(:': 'HAPPYFACE',
            ':‑)': 'HAPPYFACE',
            ':D': 'LAUGHINGFACE',
            '=D': 'LAUGHINGFACE',
            ':d': 'LAUGHINGFACE',
            '=d': 'LAUGHINGFACE',
            '.d': 'LAUGHINGFACE',
            ':-D': 'LAUGHINGFACE',
            ':(': 'UNHAPPYFACE',
            '=(': 'UNHAPPYFACE',
            ':[': 'UNHAPPYFACE',
            '.(': 'UNHAPPYFACE',
            '):': 'UNHAPPYFACE',
            ':-(': 'UNHAPPYFACE',
            ':\'(': 'CRYINGFACE',
            '=\'(': 'CRYINGFACE',
            ':\'[': 'CRYINGFACE',
            ':\'-(': 'CRYINGFACE',
            ':@': 'ANGRYFACE',
            '=@': 'ANGRYFACE',
            ':O': 'SHOCKEDFACE',
            ':o': 'SHOCKEDFACE',
            'o.o': 'SHOCKEDFACE',
            'O.o': 'SHOCKEDFACE',
            'o_O': 'SHOCKEDFACE',
            'O_o': 'SHOCKEDFACE',
            ':s': 'EMBRASEDFACE',
            ':S': 'EMBRASEDFACE',
            ' :$': 'EMBRASEDFACE',
            '=$': 'EMBRASEDFACE',
            ':/': 'CONFUSEDFACE',
            ':- *': 'KISSINGFACE',
            ': *': 'KISSINGFACE',
            ':×': 'KISSINGFACE',
            '#‑)': 'DRUNKFACE'
        }
        return emoticons

    def tag_emoticons(self, any_string):
        search_list = self.emoticons().keys()
        emo_dict = self.emoticons()

        for emo in search_list:
            pattern = "(" + emo.replace("(", "\\(").replace(")", "\\)") \
                .replace("|", "\\|").replace("[", "\\[").replace("]", "\\]") + ")"
            m = re.findall(pattern, any_string)
            if emo in m:
                any_string = any_string.replace(emo, " " + emo_dict[emo] + " ")
        return any_string